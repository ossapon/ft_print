/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ltoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 16:56:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/28 16:56:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static long int	ft_labs(long int i)
{
	return ((i < 0) ? -i : i);
}

char 		*ft_ltoa_base(long int value, int base)
{
	char	*str;
	char	*alpha;
	size_t	len;

	if (value == 0)
		return (ft_strdup("0"));
	alpha = "0123456789abcdef";
	len = (value < 0) ? (size_t)-value : (size_t)value;
	len = ft_digit_lenght(len, base);
	str = ft_strnew(len);
	while (len)
	{
		str[len-- -1] = alpha[ft_labs(value % base)];
		value /= base;
	}
	if (str[0] == '0')
		str[0] = '-';
	return (str);
}