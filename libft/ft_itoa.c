/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 23:37:19 by osapon            #+#    #+#             */
/*   Updated: 2017/11/13 23:37:21 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	int				flag;
	char			*str;
	unsigned int	i;
	size_t			len;

	flag = (n < 0) ? -1 : 1;
	len = ft_digit_lenght(((size_t)(n * flag)), 10);
	len += (flag < 0) ? 1 : 0;
	if (!(str = ft_strnew(len)))
		return (NULL);
	i = (unsigned int)(n * flag);
	str[len] = '\0';
	while (len--)
	{
		str[len] = (char) (i % 10 + '0');
		i /= 10;
	}
	if (flag == -1)
		str[0] = '-';
	return (str);
}
