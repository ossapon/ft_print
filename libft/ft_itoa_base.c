/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 17:37:19 by osapon            #+#    #+#             */
/*   Updated: 2018/02/24 17:40:21 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_abs(int i)
{
	return ((i < 0) ? -i : i);
}

char 			*ft_itoa_base(int value, int base)
{
	char 		*str;
	char 		*alpha;
	size_t 		len;

	if (value == 0)
		return (ft_strdup("0"));
	alpha = "0123456789abcdef";
	len = (value < 0) ? (size_t)-value : (size_t)value;
	len = ft_digit_lenght(len, base);
	if (!(str = ft_strnew(len)))
		return (NULL);
	while (len)
	{
		str[len-- - 1] = alpha[ft_abs(value % base)];
		value /= base;
	}
	if (str[0] == '0')
		str[0] = '-';
	return (str);
}