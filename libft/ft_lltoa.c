/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lltoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 19:36:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/28 19:36:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char 	*ft_lltoa(long long int n)
{
	char 					*str;
	int						flag;
	unsigned long long int	i;
	size_t					len;

	flag = (n < 0) ? -1 : 1;
	len = ft_digit_lenght((size_t)(n * flag), 10);
	len += (flag < 0) ? 1 : 0;
	if (!(str = ft_strnew(len)))
		return (NULL);
	i = (unsigned long long int)(n * flag);
	if (flag == -1)
		str[0] = '-';
	str[len] = '\0';
	while (len)
	{
		str[len-- -1] = (char)(i % 10 + '0');
		i /= 10;
	}
	return (str);
}
