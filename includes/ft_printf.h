/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 17:24:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/26 17:24:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# define ERROR (-1)

/*
** FLAGS DEFINE;
*/

# define HASH 1
# define ZERO 2
# define PLUS 4
# define MINUS 8
# define SPACE 16

/*
** SIZE TYPE DEFINE;
*/

# define HH 1
# define H 2
# define L 4
# define LL 8
# define J 16
# define Z 32

/*
** CONVERSIONS DEFINE;
*/

# define S_LOWERCASE 1
# define S_UPPERCASE 5
# define POINTER 9
# define D_LOWERCASE 14
# define D_UPPERCASE 19
# define INTEGER 24
# define O_LOWERCASE 29
# define O_UPPERCASE 34
# define U_LOWERCASE 39
# define U_UPPERCASE 44
# define X_LOWERCASE 49
# define X_UPPERCASE 54
# define C_LOWERCASE 59
# define C_UPPERCASE 64

/*
** ADDITIONAL LIBS;
*/

# include "../libft/libft.h"
# include <stdarg.h>
# include <stdint.h>

typedef struct	s_option
{
	char		flag;
	char		size;
	char		conversion;
	char		c;
	int			width;
	int			precision;
	char		*data;
}				t_option;

void			ft_o_manager(t_option *data);
void			ft_s_c_big_manager(t_option *data);
void			ft_s_c_small_manager(t_option *data);
void			ft_xbig_manager(t_option *data);
void			ft_xsmall_manager(t_option *data);
void			ft_decemal_manager(t_option *data);
void			ft_managing_data(t_option *data);
void			ft_parsing_data(va_list *ap, t_option *data);
void			ft_parse_spec_structure(char **format,\
				va_list *ap, int *count, t_option *data);
int				ft_printf(const char *format, ...);

#endif
