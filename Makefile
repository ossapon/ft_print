# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: osapon     <osapon@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/15 13:16:28 by osapon            #+#    #+#              #
#    Updated: 2018/02/13 14:57:22 by osapon           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

FLAGS = -Wall -Werror -Wextra

DIR_S = src

DIR_O = obj

HEADER = includes

SOURCES = ft_decemal_manager.c ft_managing_data.c \
		ft_o_manager.c ft_parse_spec_functions.c \
		ft_printf.c ft_s_c_big_manager.c ft_s_c_small_manager.c \
		ft_stdarg.c ft_xbig_manager.c ft_xsmall_manager.c

SRCS = $(addprefix $(DIR_S)/,$(SOURCES))

OBJS = $(addprefix $(DIR_O)/,$(SOURCES:.c=.o))

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(OBJS)
	make -C libft
	cp ./libft/libft.a ./$(NAME)
	ar rcs $(NAME) $(OBJS)
	echo libftprintf.a is ready

$(DIR_O):
	mkdir -p obj
	echo Creating ft_printf object files

$(DIR_O)/%.o : $(DIR_S)/%.c | $(DIR_O)
	gcc $(FLAGS) -I $(HEADER) -o $@ -c $<

clean:
	make -C libft/ clean
	rm -rf $(DIR_O)
	echo A foulder with ft_printf object files was removed

fclean: clean
	make -C libft/ fclean
	rm -fr $(NAME)
	echo libftprintf.a was removed

re: fclean all

c: clean
f: fclean