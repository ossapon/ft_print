/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_u_manager.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 00:01:00 by osapon            #+#    #+#             */
/*   Updated: 2018/03/04 00:01:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_minus_width(t_option *data, int i)
{
	char	*str;

	if (i > data->precision)
	{
		ft_memset(&(data->data[data->precision]), 0, i - data->precision);
		i -= (i - data->precision);
	}
	while (i++ < data->width)
	{
		str = data->data;
		data->data = ft_strjoin(str, " ");
		ft_strdel(&str);
	}
}

static void	ft_width(t_option *data, int i)
{
	char	*str;

	if (i > data->precision)
	{
		ft_memset(&(data->data[data->precision]), 0, i - data->precision);
		i -= (i - data->precision);
	}
	while (i++ < data->width)
	{
		str = data->data;
		data->data = ft_strjoin(" ", str);
		ft_strdel(&str);
	}
}

void		ft_s_c_small_manager(t_option *data)
{
	int		i;

	if (data->conversion == C_LOWERCASE)
	{
		data->c = (char)ft_atoi(data->data);
		data->data[0] = data->c;
		ft_bzero(&(data->data[1]), ft_strlen(&(data->data[1])));
	}
	i = ft_strlen(data->data);
	if (data->width > 0 && (data->flag & MINUS) != MINUS)
		ft_width(data, i);
	if (data->width > 0 && (data->flag & MINUS) == MINUS)
		ft_minus_width(data, i);
}
