/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 13:07:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/28 13:07:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_c_s_parser(va_list *ap, t_option *data)
{
	if (data->conversion == S_LOWERCASE)
		data->data = ft_strdup(va_arg(*ap, char*));
//	if (data->conversion == S_UPPERCASE)
//		data->data = ft_strdup(va_arg(*ap, char*));
	if (data->conversion == C_LOWERCASE)
		data->data = ft_itoa(va_arg(*ap, int));
//	if (data->conversion == C_UPPERCASE)
//		data->data = ft_c_uppercase(ap);
}

static void ft_i_base_parser(va_list *ap, t_option *data)
{
	if (data->size == HH)
		data->data = ft_itoa(va_arg(*ap, int));
	if (data->size == H)
		data->data = ft_itoa(va_arg(*ap, int));
	if (data->size == L)
		data->data = ft_ltoa(va_arg(*ap, long int));
	if (data->size == LL)
		data->data = ft_lltoa(va_arg(*ap, long long int));
	if (data->size == J)
		data->data = ft_lltoa(va_arg(*ap, intmax_t));
	if (data->size == Z)
		data->data = ft_lltoa((long long int) va_arg(*ap, size_t));
	if (data->size != HH && data->size != H && data->size != L &&
		data->size != LL && data->size != J && data->size != Z)
		data->data = ft_itoa(va_arg(*ap, int));
}

static void		ft_o_x_p_parser(va_list *ap, t_option *data)
{
	if ((data->size == L ||data->size == LL)
		&& (data->conversion == O_LOWERCASE || data->conversion == O_UPPERCASE))
		data->data = ft_lltoa_base(va_arg(*ap, long long int), 8);
	else if ((data->size == J || data->size == Z)
			&& (data->conversion == O_LOWERCASE || data->conversion == O_UPPERCASE))
		data->data = ft_ulltoa_base(va_arg(*ap, size_t), 8);
	else if ((data->size == L || data->size == LL)
			&& (data->conversion == X_LOWERCASE || data->conversion == X_UPPERCASE))
		data->data = ft_lltoa_base(va_arg(*ap, long long int), 16);
	else if ((data->size == J || data->size == Z)
			 && (data->conversion == X_LOWERCASE || data->conversion == X_UPPERCASE))
		data->data = ft_ulltoa_base(va_arg(*ap, size_t), 16);
	else if (data->conversion == X_LOWERCASE || data->conversion == X_UPPERCASE)
		data->data = ft_lltoa_base(va_arg(*ap, long long int), 16);
	else if (data->conversion == O_LOWERCASE || data->conversion == O_UPPERCASE)
		data->data = ft_lltoa_base(va_arg(*ap, long long int), 8);
	else
		data->data = ft_ulltoa_base(va_arg(*ap, unsigned long long int), 16);
}

static void	ft_u_parser(va_list *ap, t_option *data)
{
	if (data->size == L && (data->conversion == U_LOWERCASE))
		data->data = ft_lltoa((long long int) va_arg(*ap, unsigned long int));
	if (data->size == LL && (data->conversion == U_LOWERCASE))
		data->data = ft_lltoa(va_arg(*ap, unsigned long long int));
	if (data->size == J && (data->conversion == U_LOWERCASE))
		data->data = ft_lltoa((long long int) va_arg(*ap, uintmax_t));
	if (data->size == Z && (data->conversion == U_LOWERCASE))
		data->data = ft_lltoa((long long int) va_arg(*ap, size_t));
	if (data->size == L && (data->conversion == U_UPPERCASE))
		data->data = ft_lltoa((long long int) va_arg(*ap, unsigned long int));
	if (data->size == LL && (data->conversion == U_UPPERCASE))
		data->data = ft_lltoa(va_arg(*ap, unsigned long long int));
	if (data->size == J && (data->conversion == U_UPPERCASE))
		data->data = ft_lltoa((long long int) va_arg(*ap, uintmax_t));
	if (data->size == Z && (data->conversion == U_UPPERCASE))
		data->data = ft_lltoa((long long int) va_arg(*ap, size_t));
	if ((data->size != HH && data->size != H && data->size != L &&
		data->size != LL && data->size != J && data->size != Z)
		&& (data->conversion == U_LOWERCASE || data->conversion == U_UPPERCASE))
		data->data = ft_ulltoa_base(va_arg(*ap, unsigned int), 10);
}

void		ft_parsing_data(va_list *ap, t_option *data)
{
	if (data->conversion == S_LOWERCASE || data->conversion == S_UPPERCASE
		|| data->conversion == C_LOWERCASE || data->conversion == C_UPPERCASE)
		ft_c_s_parser(ap, data);
	if (data->conversion == D_LOWERCASE
		|| data->conversion == D_UPPERCASE || data->conversion == INTEGER)
		ft_i_base_parser(ap, data);
	if (data->conversion == O_LOWERCASE || data->conversion == O_UPPERCASE
		|| data->conversion == X_LOWERCASE || data->conversion == X_UPPERCASE
		|| data->conversion == POINTER)
		ft_o_x_p_parser(ap, data);
	if (data->conversion == U_LOWERCASE || data->conversion == U_UPPERCASE)
		ft_u_parser(ap, data);
}
