/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_spec_functions.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 13:04:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/28 13:04:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ft_flag(char **format, t_option *data)
{
	(*format)++;
	while (**format == '#' || **format == '0' || **format == '+'
		   || **format == '-' || **format == ' ')
	{
		if (**format == '#')
			data->flag |= HASH;
		else if (**format == '0')
			data->flag |= ZERO;
		else if (**format == '-')
			data->flag |= MINUS;
		else if (**format == '+')
			data->flag |= PLUS;
		else if (**format == ' ')
			data->flag |= SPACE;
		(*format)++;
	}
}

static void		ft_width_and_precision(char **format, t_option *data)
{
	int		i;

	i = 0;
	while (ft_isdigit((*format)[i]))
		i++;
	data->width = ft_atoi(*format);
	*format += i;
	if (**format == '.')
	{
		(*format)++;
		i = 0;
		while (ft_isdigit((*format)[i]))
			i++;
		data->precision = ft_atoi(*format);
		*format += i;
	}
}

static void		ft_size(char **format, t_option *data)
{
	while (**format == 'h' || **format == 'l'
		   || **format == 'j' || **format == 'z')
	{
		if (**format == 'h' && (*format)[1] == 'h')
			data->size |= HH;
		else if (**format == 'h' && (*format)[1] != 'h')
			data->size |= H;
		else if (**format == 'l' && (*format)[1] == 'l')
			data->size |= LL;
		else if (**format == 'l' && (*format)[1] != 'l')
			data->size |= L;
		else if (**format == 'j')
			data->size |= J;
		else if (**format == 'z')
			data->size |= Z;
		while (**format == 'h' || **format == 'l' || **format == 'j' || **format == 'z')
			(*format)++;
	}
}

static void		ft_conversion(char **format, t_option *data)
{
	if (**format == 's' || **format == 'S')
		data->conversion |= (**format == 's') ? S_LOWERCASE : S_UPPERCASE;
	else if (**format == 'p')
		data->conversion |= POINTER;
	else if (**format == 'd' || **format == 'D')
		data->conversion |= (**format == 'd') ? D_LOWERCASE : D_UPPERCASE;
	else if (**format == 'i')
		data->conversion |= INTEGER;
	else if (**format == 'o' || **format == 'O')
		data->conversion |= (**format == 'o') ? O_LOWERCASE : O_UPPERCASE;
	else if (**format == 'u' || **format == 'U')
		data->conversion |= (**format == 'u') ? U_LOWERCASE : U_UPPERCASE;
	else if (**format == 'x' || **format == 'X')
		data->conversion |= (**format == 'x') ? X_LOWERCASE : X_UPPERCASE;
	else if (**format == 'c' || **format == 'C')
		data->conversion |= (**format == 'c') ? C_LOWERCASE : C_UPPERCASE;
	(*format)++;
}

void	ft_parse_spec_structure(char **format, va_list *ap, int *count, t_option *data)
{
/*
** 1)flags;
** 2)width && precision;
** 3)size type;
** 4)conversion;
** 5)parsing incoming data according with types && size;
** 6)managing data with modifications;
** 7)print && count;
*/
	data->width = 0;
	data->precision = 0;
	data->conversion = 0;
	data->flag = 0;
	data->size = 0;
 	ft_flag(format, data);
	ft_width_and_precision(format, data);
	ft_size(format, data);
	ft_conversion(format, data);
	ft_parsing_data(ap, data);
	ft_managing_data(data);
	*count += ft_putstr(data->data);
 }
