/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_managing_data.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 13:37:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/28 13:37:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_managing_data(t_option *data)
{
	if (data->conversion == X_LOWERCASE || data->conversion == POINTER)
		ft_xsmall_manager(data);
	if (data->conversion == X_UPPERCASE && (data->flag & HASH) == HASH)
		ft_xbig_manager(data);
	if (data->conversion == D_LOWERCASE || data->conversion == D_UPPERCASE
		|| data->conversion == INTEGER)
		ft_decemal_manager(data);
	if (data->conversion == S_LOWERCASE || data->conversion == C_LOWERCASE)
		ft_s_c_small_manager(data);
//	if (data->conversion == S_UPPERCASE || data->conversion == C_UPPERCASE)
//		ft_s_c_big_manager(data);
	if (data->conversion == O_LOWERCASE || data->conversion == O_UPPERCASE)
		ft_o_manager(data);
}
