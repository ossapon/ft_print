/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 17:24:00 by osapon            #+#    #+#             */
/*   Updated: 2018/02/26 17:24:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"


int				ft_printf(const char *format, ...)
{
	va_list		ap;
	t_option	data;
	int			count;

	va_start(ap, format);
	count = 0;
	data.width = 0;
	data.precision = 0;
	data.flag = 0;
	while (*format)
	{
		/*
		** begin of format sign and parsing data structure;
		*/
		if (*format == '%' && format[1] != '%')
			ft_parse_spec_structure((char **) &format, &ap, &count, &data);
		else
		{
			/*
			** managing  "%%"
			*/
			if (*format == '%' && format[1] == '%')
				format++;
			/*
			**printing [format] chars;
			*/
			count += ft_putchar(*format++);
		}
	}
	va_end(ap);
	return (count);
}