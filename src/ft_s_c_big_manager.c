/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_s_c_big_manager.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 03:49:00 by osapon            #+#    #+#             */
/*   Updated: 2018/03/04 03:49:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_data_manager(t_option *data, void *new)
{
	unsigned int c;

	c = (unsigned int)new;
	data->data = ft_strnew(4);
	if (c < 0x80)
		data->data[0] = (unsigned char) c;
	else if (c < 0x7ff)
	{
		data->data[0] = (unsigned char) (0xc0 | (c >> 6));
		data->data[1] = (unsigned char) (0x80 | (0x3f & c));
	}
	else if (c < 0xffff)
	{
		data->data[0] = (unsigned char) (0xe0 | (c >> 12));
		data->data[1] = (unsigned char) (0x80 | (0x3f & (c >> 6)));
		data->data[2] = (unsigned char) (0x80 | (0x3f & c));
	}
	else if (c < 0x1fffff)
	{
		data->data[0] = (unsigned char) (0xf0 | (c >> 18));
		data->data[1] = (unsigned char) (0x80 | (0x3f & (c >> 12)));
		data->data[2] = (unsigned char) (0x80 | (0x3f & (c >> 6)));
		data->data[3] = (unsigned char) (0x80 | (0x3f & c));
	}
	return (data->data);
}

static void	ft_width(t_option *data, int i)
{
	char	*str;

	while (i++ < data->width)
	{
		str = data->data;
		data->data = ft_strjoin(" ", str);
		ft_strdel(&str);
	}
}

static void	ft_minus_width(t_option *data, int i)
{
	char	*str;

	while (i++ < data->width)
	{
		str = data->data;
		data->data = ft_strjoin(str, " ");
		ft_strdel(&str);
	}
}

void		ft_s_c_big_manager(t_option *data)
{
	int				i;
	unsigned int	s;

	i = 1;
	s = (unsigned int)(data->data);
	if (s < 0x80)
		i = 1;
	else if (s < 0x7ff)
		i = 2;
	else if (s < 0xffff)
		i = 3;
	else if (s < 0x1fffff)
		i = 4;
	if (data->width > 0 && (data->flag & MINUS) != MINUS)
		ft_width(data, i);
	if (data->width > 0 && (data->flag & MINUS) == MINUS)
		ft_minus_width(data, i);
}
