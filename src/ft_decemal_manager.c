/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_decemal_manager.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 22:11:00 by osapon            #+#    #+#             */
/*   Updated: 2018/03/03 22:11:00 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_width(t_option *data, int i)
{
	char	*str;

	while (i++ < data->width)
	{
		str = data->data;
		data->data = ft_strjoin(str, " ");
		ft_strdel(&str);
	}
}

static void	ft_precision(t_option *data, char **tmp, size_t i)
{
	char	*str;

	i = (data->data[0] == '-') ? data->precision - i + 1 : (size_t)data->precision - i;
	data->data[0] = (char) ((*tmp[0] == '-') ? '0' : data->data[0]);
	while (i--)
	{
		str = data->data;
		data->data = ft_strjoin("0", str);
		ft_strdel(&str);
	}
	data->data[0] = (char) ((*tmp[0] == '-') ? '-' : data->data[0]);
}

void		ft_decemal_manager(t_option *data)
{
	char	*tmp;
	int		i;

	tmp = ft_strdup(data->data);
	i = ft_strlen(data->data);
	if ((data->flag & PLUS) != PLUS && (data->flag & MINUS) != MINUS
		&& (data->flag & SPACE) != SPACE && (data->flag & ZERO) != ZERO)
		ft_width(data, i);
	else
		ft_precision(data, &tmp, i);
	if ((data->flag & PLUS) == PLUS && data->data[0] != '-')
		data->data = ft_strjoin("+", data->data);
	if ((data->flag & SPACE) == SPACE && (data->flag & PLUS) != PLUS
		&& data->data[0] != '-')
		data->data = ft_strjoin(" ", data->data);
	if ((data->flag & MINUS) == MINUS && data->width > data->precision)
		ft_width(data, ft_strlen(data->data));
	ft_strdel(&tmp);
}

